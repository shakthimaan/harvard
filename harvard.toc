\contentsline {section}{\numberline {1}Quick Reference}{1}%
\contentsline {subsection}{\numberline {1.1}Single Table}{2}%
\contentsline {subsection}{\numberline {1.2}Multiple Tables}{2}%
\contentsline {subsection}{\numberline {1.3}Arrows}{3}%
\contentsline {subsubsection}{\numberline {1.3.1}Complete Citations}{3}%
\contentsline {subsubsection}{\numberline {1.3.2}Partial Citations}{3}%
\contentsline {subsubsection}{\numberline {1.3.3}Parenthesis Styles}{3}%
\contentsline {section}{\numberline {2}Introduction}{3}%
\contentsline {section}{\numberline {3}Citations}{4}%
\contentsline {subsection}{\numberline {3.1}Complete Citations}{4}%
\contentsline {subsection}{\numberline {3.2}Citation Modes}{5}%
\contentsline {subsection}{\numberline {3.3}Partial Citations}{5}%
\contentsline {subsection}{\numberline {3.4}Exceptions for Individual Citations}{6}%
\contentsline {section}{\numberline {4}Styles}{6}%
\contentsline {subsection}{\numberline {4.1}Bibliography Styles}{6}%
\contentsline {subsection}{\numberline {4.2}Citation Styles}{6}%
\contentsline {subsection}{\numberline {4.3}Parenthesis Style}{7}%
\contentsline {subsection}{\numberline {4.4}Conjunction Style}{7}%
\contentsline {section}{\numberline {5}World Wide Web (WWW) References}{7}%
\contentsline {section}{\numberline {6}Doing It By Hand}{8}%
\contentsline {section}{\numberline {7}Acknowledgement}{8}%
